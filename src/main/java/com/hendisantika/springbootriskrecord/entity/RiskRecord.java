package com.hendisantika.springbootriskrecord.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-risk-record
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/03/20
 * Time: 11.17
 */
@Entity
@Table(name = "risk_record")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RiskRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long issueId;
    private String issueName;

    private int impactRating;

    private String reporterName;

    private LocalDateTime dateAdded;
}
