package com.hendisantika.springbootriskrecord.repository;

import com.hendisantika.springbootriskrecord.entity.RiskRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-risk-record
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/03/20
 * Time: 11.19
 */
@Repository
public interface RiskRecordRepository extends JpaRepository<RiskRecord, Long> {
}