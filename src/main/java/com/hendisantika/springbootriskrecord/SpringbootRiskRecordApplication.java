package com.hendisantika.springbootriskrecord;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRiskRecordApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRiskRecordApplication.class, args);
    }

}
