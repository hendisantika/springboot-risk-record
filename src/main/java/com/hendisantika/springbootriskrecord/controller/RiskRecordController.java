package com.hendisantika.springbootriskrecord.controller;

import com.hendisantika.springbootriskrecord.entity.RiskRecord;
import com.hendisantika.springbootriskrecord.service.RiskRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-risk-record
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/03/20
 * Time: 11.21
 */
@RestController
@RequestMapping("/risk-records")
public class RiskRecordController {

    @Autowired
    private RiskRecordService riskRecordService;

    //GET all risk records
    @GetMapping
    public Collection<RiskRecord> retrieveAllRiskRecords() {
        return this.riskRecordService.retrieveAllRiskRecords();
    }

    //GET a single risk record
    @GetMapping(value = "/{issueId}")
    public Optional<RiskRecord> retrieveRecordById(@PathVariable long issueId) {
        return this.riskRecordService.retrieveRecordById(issueId);
    }

    //POST a risk record
    @PostMapping
    public RiskRecord saveRiskRecord(@RequestBody RiskRecord riskRecord) {
        return this.riskRecordService.saveRiskRecord(riskRecord);
    }

    //DELETE a single risk record
    @DeleteMapping(value = "/{issueId}")
    public void removeRecordById(@PathVariable long issueId) {
        this.riskRecordService.removeRecordById(issueId);
    }

    //PUT/UPDATE a risk record
    @PutMapping
    public RiskRecord updateRiskRecord(@RequestBody RiskRecord riskRecord) {
        return this.riskRecordService.updateRiskRecord(riskRecord);
    }
}
